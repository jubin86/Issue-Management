<section class="bg-white jumbotron">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <h2 class="text-black py-4">Let's Get In Touch!</h2>
            <hr class="my-4" id="line4">
            <p class="text-muted text-black">Ready to start your next project with us?
                That's great! Give us a call or send us an email and we will
                get back to you as soon as possible!</p>

        </div>
    </div>
    <div class="row">
        <div class="col-md-6 text-center">
            <i class="fas fa-4x fa-phone text-primary mb-3 sr-icon-4"></i>
        </div>
        <div class="col-md-6 text-center">
            <i class="fas fa-4x fa-phone text-primary mb-3 sr-icon-4"></i>
        </div>
    </div>

</section>


<script src="../js/jquery-3.3.1.js" type="text/javascript"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<script src="../js/popper.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>

<!--must be added for downloading -->

<script src="js/tableExport.js" type="text/javascript"></script>
<script src="js/jquery.base64.js" type="text/javascript"></script>

<!-- for pdf file download-->
<script type="text/javascript" src="js/jspdf/jspdf.js"></script>
<script type="text/javascript" src="js/jspdf/libs/base64.js"></script>
<script type="text/javascript" src="js/jspdf/libs/sprintf.js"></script>


<script src="../js/custom.js" type="text/javascript"></script>


