<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" media="all">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="general_style.css" media="all">
</head>


<body>
    <section class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo-header">
                        <h2 class="float-left">START BOOTSTRAP</h2>
                       <a class="float-left ml-3" style="margin-top: 31px;
                       text-decoration: none;"
                          href="signup/login.php">Login</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="menu">
                        <ul>
                            <li><a href="">Home</a></li>
                            <li><a href="signup/view.php">User</a></li>


                            <li><a href="category/category_view.php">Category</a></li>
                            <li><a href="solver/issue_solve.php">Category upload</a></li>

                            <li><a href="issue/issue_view.php">Issue</a></li>
                            <li><a href="solver/issue_solve.php">Solver</a></li>


                        </ul>
                    </div>

                </div>
            </div>
            <div class="row">
                <!--<div class="col-md-12">-->
                    <!--<div class="top-header">-->
                        <!--<h1>YOUR FAVORITE SOURCE OF<br/>-->
                            <!--FREE BOOTSTRAP THEMES</h1>-->
                        <!--<button class="border"></button>-->
                    <!--</div>-->
                    <!--<div class="header-details">-->
                        <!--<h3>Start Bootstrap can help you build better websites using the Bootstrap CSS<br/>-->
                            <!--framework! Just download your template and start going, no strings attached!</h3>-->
                    <!--</div>-->
                    <!--<div class="findout-button" id="findout">-->
                        <!--<button class="btn btn-primary">Find Out</button>-->
                    <!--</div>-->
                <!--</div>-->
                <div class="col-md-12 text-center">
                    <h2 class="section-heading text-white" id="topHeading">YOUR FAVORITE SOURCE OF<br/>
                        FREE BOOTSTRAP THEMES!</h2>
                    <hr class="light my-4" id="line2">
                    <div class="headerPara">
                    <p class="text-faded mb-4">Start Bootstrap has everything you need to get your new website up
                        and running in no time! All of the templates and themes on Start Bootstrap are open source,
                        free to download, and easy to use. No strings attached!</p>
                    </div>
                    <div class="findout-button">
                        <a class="btn btn-light btn-xl js-scroll-trigger" href="#services" id="services2">Get Started!</a>
                    </div>
                </div>
            </div>
            </div>

    </section>

    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center p-5">
                    <h2 class="section-heading text-white">We've Got What You Need!</h2>
                    <hr class="light my-4" id="line">
                    <div class="second-para my-3">
                    <p class="text-faded mb-4">Start Bootstrap has everything you need
                        to get your new website up and running in no time! All of the templates
                        and themes on Start Bootstrap are open source, free to download, and easy to use.
                        No strings attached!</p>
                    </div>
                    <a class="btn btn-light btn-xl js-scroll-trigger px-2" href="#services" id="getStarted">Get Started!</a>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading display-5 my-3">At Your Service</h2>
                    <hr class="my-4" id="line3">
                </div>
            </div>
        </div>
        <div class="container p-5">
            <div class="row">
                <div class="col-lg-3 col-md-6  col text-center" >
                    <div class="service-box mt-5 mx-auto">
                        <i class="fas fa-4x fa-gem text-primary mb-3 sr-icon-1"></i>
                        <h3 class="mb-3">Sturdy Templates</h3>
                        <p class="text-muted mb-0">Our templates are updated regularly so they don't break.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fas fa-4x fa-paper-plane text-primary mb-3 sr-icon-2"></i>
                        <h3 class="mb-3">Ready to Ship</h3>
                        <p class="text-muted mb-0">You can use this theme as is, or you can make changes!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fas fa-4x fa-code text-primary mb-3 sr-icon-3"></i>
                        <h3 class="mb-3">Up to Date</h3>
                        <p class="text-muted mb-0">We update dependencies to keep things fresh.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box mt-5 mx-auto">
                        <i class="fas fa-4x fa-heart text-primary mb-3 sr-icon-4"></i>
                        <h3 class="mb-3">Made with Love</h3>
                        <p class="text-muted mb-0">You have to make your websites with love these days!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-dark ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-xs-1 p-0">
                    <img src="images/1.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-4 col-lg-4 col-xs-1 p-0">
                    <img src="images/2.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-4 col-lg-4 col-xs-1 p-0">
                    <img src="images/3.jpg" class="img-fluid" alt="">
                </div>

            </div>

            <div class="row">
                <div class="col-md-4 col-lg-4 col-xs-1 p-0">
                    <img src="images/4.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-4 col-lg-4 col-xs-1 p-0">
                    <img src="images/5.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-4 col-lg-4 col-xs-1 p-0">
                    <img src="images/6.jpg" class="img-fluid" alt="">
                </div>

            </div>
        </div>
    </section>

<section class="bg-dark jumbotron"><!...download section..>
    <div class="row">
        <div class="col-md-12 col-lg-12
 col-xs-12 text-center">
            <h2 class="text-white py-4">Free Download at Start Bootstrap!</h2>
            <button class="btn btn-primary">Download Now</button>
        </div>
    </div>

</section>
 <section class="bg-white jumbotron">
     <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12 text-center">
             <h2 class="text-black py-4">Let's Get In Touch!</h2>
             <hr class="my-4" id="line4">
             <p class="text-muted text-black">Ready to start your next project with us?
                 That's great! Give us a call or send us an email and we will
                 get back to you as soon as possible!</p>

         </div>
     </div>
     <div class="row">
         <div class="col-md-6 text-center">
             <i class="fas fa-4x fa-phone text-primary mb-3 sr-icon-4"></i>
         </div>
         <div class="col-md-6 text-center">
             <i class="fas fa-4x fa-phone text-primary mb-3 sr-icon-4"></i>
         </div>
     </div>

 </section>

</body>
</html>