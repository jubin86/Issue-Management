<?php
include_once('../session.php');

class User
{

    public $name = "";
    public $email = "";
    public $username = "";
    public $password = "";
    public $id = "";
    public $group_id = "";
    public $category_id = null;
    public $file_name=null;
    private $pdo = null;

    public function __construct()
    {

        $this->pdo = new PDO('mysql:host=localhost;dbname=swapnoloke', 'root', '');

    }

    public function uploadImage($files = null){
        if (!empty($files['image']['name'])) {
            $filename = $files['image']['name'];

            $filepath = $files['image']['tmp_name'];
            move_uploaded_file($filepath, "images/" . $filename);
            echo "image uploaded success";
            $_POST['image'] = $filename;

        }else{
            $_POST['image'] = '';
        }
        return $_POST;

    }





    public function getData($data = null)
    {


        if (!empty($data['group_id'])) {
            $this->group_id = $data['group_id'];
        }
        if (!empty($data['category_id'])) {
            $this->category_id = $data['category_id'];
        }

        if (!empty($data['name'])) {

            $this->name = filter_var($data['name'], FILTER_SANITIZE_STRING);
        }
        if (!empty($data['email'])) {
            $this->email = $data['email'];
        }
        if (!empty($data['username'])) {
            $this->username = $data['username'];
        }
        if (!empty($data['password'])) {
            $this->password = $data['password'];
        }

        if (!empty($_POST['image'])) {
            $this->file_name = $data['image'];
        }
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

//        if ($this->name='' OR $this->username='' OR $this->email='' OR $this->password='' ){
//            $alertmsg= "<div class='alert alert-danger'>this fill is must be fillup</div>";
//            return $alertmsg;
//        }


    }

    public function insertData()
    {
        try {


            $query = "INSERT INTO registration(name,email,username,password,group_id,category_id,file_name) VALUES
(:anyname,:anyemail,:anyusername,:anypassword,:group_id,:category_id,:file_name)";

            $ss = $this->pdo->prepare($query);


            $test = $ss->execute(array(
                ':anyname' => $this->name,
                ':anyemail' => $this->email,
                ':anyusername' => $this->username,
                ':anypassword' => $this->password,
                ':group_id' => $this->group_id,
                ':category_id'=>$this->category_id,
                ':file_name'=>$this->file_name,

            ));
//            echo '<pre>';
//            print_r($test);
//            echo '<pre>';


            if ($test) {


                header("Location:view.php");

            }
        } catch (PDOException $e) {
            echo 'Error:0 ' . $e->getmessage();
        }


    }

    public function store()
    {
        try {
            $query = "SELECT * FROM registration";
            $aa = $this->pdo->query($query);

            $aa->execute();
            $test = $aa->fetchAll(PDO::FETCH_ASSOC);

            return $test;
        } catch (PDOException $e) {
            echo 'Error:0' . $e->getMessage();
        }


    }

    public function details($id = '')
    {
        try {
            $query = "SELECT * FROM registration where id=$id LIMIT 1";
            $fetch = $this->pdo->query($query);
//            $fetch->execute();

            $data = $fetch->fetchAll(PDO::FETCH_ASSOC);
            return $data[0];


        } catch (PDOException $e) {
            echo 'Error:0' . $e->getMessage();
        }
    }

    public function update()
    {

        try {
            $query = "UPDATE registration SET name=:name,username=:username,email=:email where id=$this->id";

            $update = $this->pdo->prepare($query);


            $update->execute(array(
                ':name' => $this->name,
                ':username' => $this->username,
                ':email' => $this->email,


            ));
            if ($update) {
                header('location:view.php');
            } else {
                echo "something wrong";
            }


        } catch (PDOException $e) {
            echo 'error:0' . $e->getMessage();
        }
    }

    public function delete($id = '')
    {

        try {
            $query = "DELETE FROM registration where id=$id";

            $delete = $this->pdo->query($query);
            if ($delete) {
                header('location:view.php');

            } else {
                echo "something wrong";
            }


        } catch (PDOException $e) {
            echo 'errpr:0' . $e->getMessage();
        }
    }


    public function checkLogin($data = '')
    {

        $email = $data['email'];
        $password = $data['password'];


        $query = "SELECT * FROM registration where email='$email' and password='$password'";
        $fetch = $this->pdo->query($query);

//        $fetch->execute();
        $data = $fetch->fetch(PDO::FETCH_ASSOC);


        return $data;

//            if (!empty($data)){
//                $_SESSION['userdata'] = $data;
//                $_SESSION['success'] = "login approval correct";
//                header('location:view.php');
//            }
//
//            else {
//                $_SESSION['message'] = "you are not authorized";
//                header('location:login.php');
//            }


    }

    public function pathLoginUser($value)
    {

//
        if ($value) {
            $_SESSION['userdata'] = $value;
            $_SESSION['success'] = "login approval correct";

            if ($_SESSION['userdata']['group_id']==1){
                header('location:view.php');
            }
            elseif ($_SESSION['userdata']['group_id']==2){

                header('location:../issue/issue_view.php');
            }
            elseif ($_SESSION['userdata']['group_id']==3){

                header('location:../solver/issue_solve.php');
            }


        }



        else {
            $_SESSION['message'] = "you are not authorized";


            header('location:login.php');
        }

    }

    /*public function requestUserCheck($value_issue)
    {

        $this->checkLogin($data = '');


        $query = "SELECT * FROM registration where email='$email' and password='$password' and group_id=1";

        $fetch = $this->pdo->query($query);

//        $fetch->execute();

        $data = $fetch->fetch(PDO::FETCH_ASSOC);


        return $data;


        if ($data and $data['group_id'] == 2) {
//            $_SESSION['requesterData']=$data;
//            $_SESSION['requesterSuccess']='approved requester';
            header('location:issue_view.php');
        } else {
            $_SESSION['requesterUnuccess'] = 'wrong are wrong requester';
            header('location:login.php');
        }

    }*/













//
//    public function dd($data=''){
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        die();
//    }




}
