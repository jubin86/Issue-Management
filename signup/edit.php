<?php
//require_once ('user.php');
//$obj=new User();
//$obj->getData($_POST);
//$obj->update();
//
//
include_once ('../session.php');
include_once('../header.php');

require_once ('user.php');
$obj = new User();

$id = $_GET['id'];

$result = $obj->details($id);

    if(!isset($_SESSION['userdata'])){
    header('location:login.php');
    }

?>



    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="category w-25 mt-3">
                    <button class=" btn btn-primary">
                        <a href="view.php" class="text-white">Back</a>
                    </button>
                </div>

            </div>
        </div>
        <div class="row main">

            <div class=main-form my-5 style="margin: 0px auto">

                <form class="form-horizontal" method="post" action="update.php">

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Your Name</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>

                            <input type="text" class="form-control" name="name" id="name"
                                   placeholder="Enter your Name" value="<?php echo $result['name'] ?>"/>
                        </div>

                    </div>


                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label">Username</label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="username" id="username"
                                   placeholder="Username" value="<?php echo $result['username'] ?>"/>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label">Email</label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="email" id="email"
                                   placeholder="abc@gmail.com" value="<?php echo $result['email'] ?>"/>
                        </div>

                    </div>

                    <input type="hidden" class="form-control" name="id" value="<?php echo $result['id'] ?>"/>


                    <div class="form-group ">
                        <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    </body>
    </html>


<?php

include_once('../footer.php');
?>