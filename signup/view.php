<?php
//require_once ('user.php');
//    $obj=new User();
//$data=$obj->showData();
//session_start();
include_once('../session.php');

include_once('../header.php');
require_once('user.php');
$obj = new User();

$dataArray = $obj->store();

//$singleUser=$obj->details();
//echo '<pre>';
//print_r($singleUser);






if (!isset($_SESSION['userdata'])) {
    header('location:login.php');
}
if ($_SESSION['userdata']['group_id'] == 2) {
    header('location:login.php');
}
if ($_SESSION['userdata']['group_id'] == 3) {
    header('location:login.php');
}


?>


    <body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6">
                <div class="category mt-3">

                    <a href="../index.php" class="text-dark">Back</a>

                </div>

            </div>
        </div>

        <!--    <button class="btn btn-info text-white"><a href="logout.php">Logout</a></button>-->
        <!--    <h2 class="text-center">User Information</h2>-->
        <div class="row">
            <div class="col-md-6">
                <div class="category1 float-left">
                    <button class=" btn btn-primary">
                        <a href="registration_form.php" class="text-white" style="text-decoration: none">Register</a>
                    </button>
                </div>
                <div class="category2 float-left ml-3">
                    <button class=" btn btn-primary">
                        <a href="login.php" class="text-white" style="text-decoration: none">Login</a>
                    </button>
                </div>
            </div>
            <div class="col-md-6">
                <div class="category3 float-right ml-3">
                    <button class=" btn btn-primary">
                        <a href="../solver/solving_record.php" class="text-white " style="text-decoration: none">Solving Record</a>
                    </button>
                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>User Information</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <h3><?php

                    if (isset($_SESSION['userdata'])) {
//            echo $_SESSION['success'];
//            unset($_SESSION['success']);
                        echo "<span>Welcome to </span>" . $_SESSION['userdata']['name'];
                    }
                    ?></h3>
            </div>
        </div>


<!--        <h2>--><?php
//            //            print_r($_SESSION['userdata']);
//            //
//            //        if (!empty($_SESSION['userdata'])) {
//            //            echo "<span>Welcome to </span>" . $_SESSION['userdata']['name'];
//            ////            unset($_SESSION['userdata']);
//            //        }
//
//            ?><!--</h2>-->


        <table class="table border-info" border="1">
            <thead>

            <tr>
                <th>Indentity</th>
                <th>Name</th>
                <th>Email</th>
                <th>UserName</th>
                <th>Photo</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($dataArray as $item) { ?>
                <tr>
                    <td><p class="text-center mt-4"><?php echo $item['group_id'] ?></p></td>
                    <td><p class="text-center mt-4"><?php echo $item['name'] ?></p></td>
                    <td><p class="text-center mt-4"><?php echo $item['email'] ?></p></td>
                    <td><p class="text-center mt-4"><?php echo $item['username']?></p></td>

                    <td>
                        <?php if ($item['file_name']){?>
                            <img src="images/<?php echo $item['file_name'];?>"
                             alt="" class="w-auto ml-4" height="80px" style="border-radius:34%;">

                      <?php  }
                      else{
                            ?>
                          <img src="../images/default.jpg" alt="" class="w-auto ml-4" height="80px" width="100px" style="border-radius:34%;">
                     <?php }
//                      ?>
                    </td>
                    <td>
                        <a class="ml-2" href="details.php?id=<?php echo $item['id'] ?>" style="text-decoration: none">

                            <i class="fas fa-info mt-4"></i>
                        </a>
                        <a class="ml-2" href="edit.php?id=<?php echo $item['id'] ?>" style="text-decoration: none">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a class="ml-2" href="delete.php?id=<?php echo $item['id'] ?>" style="text-decoration: none">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    </body>
    </html>

<?php
include_once('../footer.php');

?>