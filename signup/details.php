<?php
include_once ('../session.php');
include_once ('../header.php');

require_once ('user.php');;
$obj = new User();
$id = $_GET['id'];
$result = $obj->details($id);

if (!isset($_SESSION['userdata'])) {
    header('location:login.php');
}

?>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-6">
            <div class="category w-25 mt-3">
                <button class=" btn btn-primary">
                    <a href="view.php" class="text-white">Back</a>
                </button>
            </div>

        </div>
    </div>
    <h2> Information of this issue</h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Username</th>
            <th>Password</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php echo $result['id'];?></td>
            <td><?php echo $result['name'];?></td>
            <td><?php echo $result['email'];?></td>
            <td><?php echo $result['username'];?></td>
            <td><?php echo $result['password'];?></td>
        </tr>


        </tbody>
    </table>
</div>







