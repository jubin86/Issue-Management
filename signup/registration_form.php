<?php
include_once('../header.php');
include_once ('../category/Category.php');
$obj=new Category();
$data=$obj->view();



?>

    <body>

    <div class="container">
        <div class="row main">


            <div class="main-form my-5" style="margin: 0px auto">


                <form class="form-horizontal" method="post" action="registration.php" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label" style="margin-left: 25px">Choose Identity</label>

                        <div class="input-group">
                            <span class="input-group-addon mr-3"><i class="fas fa-id-card" aria-hidden="true"></i></span>

                            <select name="group_id" id="groupId" class="form-control">
                                <option value="0">select</option>
                                <option value="1">Admin</option>
                                <option value="2">Requester</option>
                                <option value="3">Solver</option>
                            </select>
                        </div>
                    </div>

                    <div id="showCategory" style="display: none">

                    <div class="input-group">
                        <select name="category_id" id="categoryId" class="form-control ml-4">
                            <option value="0">select</option>

                            <?php foreach ($data as $find_all){ ?>
                            <option value="<?php echo $find_all['id'];?>"><?php echo $find_all['name'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label" style="margin-left: 25px">Your Name</label>

                        <div class="input-group">
                            <span class="input-group-addon mr-3"><i class="fa fa-user fa" aria-hidden="true"></i></span>

                            <input type="text" class="form-control" name="name" id="name"
                                   placeholder="Enter your Name" value="<?php //echo $name;?>"/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="email" class="cols-sm-2 control-label" style="margin-left: 25px">Your Email</label>

                        <div class="input-group">
                            <span class="input-group-addon mr-3"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="email" id="email"
                                   placeholder="Enter your Email"/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label" style="margin-left: 33px">Username</label>

                        <div class="input-group">
                            <span class="input-group-addon mr-3"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="username" placeholder="Enter your Username"/>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label" style="margin-left: 25px">Password</label>

                        <div class="input-group">
                            <span class="input-group-addon mr-3"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>

                            <input type="password" class="form-control" name="password">
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="image" class="col-sm-2 control-label" style="margin-left: 25px">image</label>

                        <div class="input-group">
                            <span class="input-group-addon mr-3"><i class="far fa-image" aria-hidden="true"></i></i></span>

                            <input type="file" class="form-control" name="image">
                        </div>

                    </div>

                    <div class="form-group mt-md-4 ml-md-4  ">
                        <button type="submit" name="submit" class="btn btn-primary ">Register</button>
                        <a class="ml-2" href="login.php" style="text-decoration: none">Login</a>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    </body>
    </html>


<?php
include_once('../footer.php');

?>