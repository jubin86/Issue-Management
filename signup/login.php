<?php
include_once('../session.php');
include_once('../header.php');


?>

<body>


<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <p class="mt-md-4">

                <?php
                if (!empty($_SESSION['message'])) {
                    echo "<h2>" . $_SESSION['message'] . "</h2>";
                    unset($_SESSION['message']);

                }

                if (!empty($_SESSION['logout'])) {
                    echo '<h2>' . $_SESSION['logout'] . '</h2>';
                    unset($_SESSION['logout']);
                }
                ?>
            </p>
        </div>

    </div>

    <div id="login-row" class="row justify-content-center align-items-center">

        <div id="login-column" class="col-md-6">
            <div class="box">


                <div class="shape1"></div>
                <div class="shape2"></div>
                <div class="shape3"></div>
                <div class="shape4"></div>
                <div class="shape5"></div>
                <div class="shape6"></div>
                <div class="shape7"></div>
                <div class="float">

                    <form class="form" action="user_login.php" method="post">
                        <div class="form-group">
                            <label for="email" class="text-white">Email:</label><br>
                            <input type="email" name="email" id="email" class="form-control"
                                   placeholder="jon@gmail.com">
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-white">Password:</label><br>
                            <input type="text" name="password" id="password" class="form-control" autocomplete="off"
                                   placeholder="password">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">

                            <a class="ml-md-3 text-right font-weight-normal text-dark text-justify"
                               href="registration_form.php"
                               style="text-decoration: none;font-size: 18px">Registration</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>