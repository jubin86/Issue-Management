<?php
include_once('../session.php');

class Solver
{

    public $date = "";
    public $progress = "";
    public $remarks = "";
//    public $id="";
    public $issue_id = "";
    public $fromDate = "";
    public $toDate = "";
    public $fromDateObj = "";
    public $toDateObj = "";
//    public $start="";
//    public $end="";

//    public $dateArray= null;
//    public $diff='';


    public function __construct()
    {

        $this->pdo = new PDO('mysql:host=localhost;dbname=swapnoloke', 'root', '');
    }

    public function setData($data = '')
    {


        if (!empty($data['date'])) {
            $this->date = $data['date'];
        }

        if (!empty($data['progress'])) {
            $this->progress = $data['progress'];
        }
        if (!empty($data['remarks'])) {
            $this->remarks = $data['remarks'];
        }
//        if(!empty($data['id'])){
//            $this->id= $data['id'];
//        }

        if (!empty($data['issue_id'])) {
            $this->issue_id = $data['issue_id'];
        }


    }

    public function insertData()
    {
        try {


            $query = "INSERT INTO issue_track(date,progress,remarks,issue_id,solver_id) VALUES
                    (:date,:progress,:remarks,:issue_id,:solver_id)";

            $ss = $this->pdo->prepare($query);


            $test = $ss->execute(array(
                ':date' => $this->date,
                ':progress' => $this->progress,
                ':remarks' => $this->remarks,
                ':issue_id' => $this->issue_id,
                ':solver_id' => $_SESSION['userdata']['id'],


            ));


        } catch (PDOException $e) {
            echo 'error:0' . $e->getMessage();
        }
    }

    public function index()
    {
        try {

            $query = "SELECT * FROM issue_track";
            $data = $this->pdo->query($query);

            $data->execute();
            $test = $data->fetchAll(PDO::FETCH_ASSOC);
            return $test;


        } catch (PDOException $e) {
            echo 'error:0' . $e->getMessage();
        }
    }

    public function getIssueTrackInfoById($id)
    {
        try {
            $sql = "select * from issue_track where issue_id='" . $id . "'";
            $data = $this->pdo->query($sql);
            $data->execute();
            $rows = $data->fetchAll(PDO::FETCH_ASSOC);
            return $rows;
        } catch (PDOException $e) {
            return 'error:0' . $e->getMessage();
        }

    }


    public function getDataByissueId($id)
    {
        try {
            $query = "SELECT * FROM issue_track where issue_id='" . $id . "'";
            $data = $this->pdo->query($query);

            $data->execute();
            $result = $data->fetchAll(PDO::FETCH_ASSOC);
            return $result;
//            echo '<pre>';
//            print_r($result);exit();
        } catch (PDOException $e) {
            return 'error:0' . $e->getMessage();
        }


    }

    public function solveRecordByDate($date = null)
    {
        try {
            if (empty($date['from_date']) ||empty($date['to_date'])){
                $this->fromDate = $date['from_date'];
                $this->toDate = $date['to_date'];
            }
            else{
                $this->fromDate = $date['from_date'];
                $this->toDate = $date['to_date'];
            }





            $this->fromDateObj = date_create($date['from_date']);

            $this->toDateObj = date_create($date['to_date']);


            $diff = date_diff($this->fromDateObj, $this->toDateObj);
            $day = $diff->format("%a");

            $dateArray = array();

            for ($i = 0; $i <= $day; $i++) {
                $dateArray[$i] = $this->fromDate;

                $this->fromDate = date('Y-m-d', strtotime("+1 day", strtotime($this->fromDate)));


            }
            /*$query="SELECT * FROM issue_track WHERE date BETWEEN date = :fromDateObj and date = :toDateObj";
            $data=$this->pdo->prepare($query);
//        echo '<pre>';
//        print_r($data);exit();

//        $data->bindParam(':fromDateObj', $this->fromDate, PDO::PARAM_STR);
//        $data->bindParam(':toDateObj', $this->toDate, PDO::PARAM_STR);

            $data->execute();
            $fetch=$data->fetchAll(PDO::FETCH_ASSOC);
            echo '<pre>';
            print_r($fetch);*/



            return $dateArray;


        } catch (PDOException $e) {
            return 'error:0' . $e->getMessage();
        }


    }
    public function dateSearching($dateArray=null){


        $start=$dateArray[0];


        $end=end($dateArray);

        $sql = "SELECT * FROM `issue_track` WHERE `date` BETWEEN '".$start."' AND '".$end."'";
        $stmt = $this->pdo->prepare($sql);
//        $stmt->bindParam(':fromDateObj', $this->fromDate,
//            PDO::PARAM_STR);
//        $stmt->bindParam(':toDateObj', $this->toDate,
//            PDO::PARAM_STR);

        $stmt->execute();
        $data=$stmt->fetchAll(PDO::FETCH_ASSOC);

        return $data;
//            $total = $stmt->rowCount();
        /*while ($row = $stmt->fetchObject()) {
                echo "$row->solver_id";
                echo "$row->issue_id";
                echo "$row->date";
                echo "$row->progress";

            echo '<pre>';
            print_r($row);
        }*/

    }

    public function solveCountQuery(){

        $sql="SELECT COUNT(id) AS total, solver_id, date FROM issue_track 
              GROUP BY solver_id, DATE ORDER BY DATE, solver_id";
        $data=$this->pdo->query($sql);

        $data->execute();
        $result=$data->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getSolverWithCategory(){

        /*$sql="SELECT issue_track.solver_id,registration.name as solver_name, category.name as category_name
        FROM issue_track 
        INNER JOIN registration 
        ON registration.id=issue_track.solver_id 
        INNER JOIN issue 
        ON issue.id=issue_track.issue_id 
        INNER JOIN category 
        ON category.id=issue.category_id";*/

        $sql="SELECT registration.id, registration.name as solver_name, category.name as category_name 
              FROM registration inner join category on 
              registration.category_id =category.id";

        $data=$this->pdo->query($sql);

        $data->execute();
        $result=$data->fetchAll(PDO::FETCH_ASSOC);
        return $result;

    }
    public function singleCount($id=null){

        $sql="SELECT COUNT(issue_id) as total FROM  issue_track
              WHERE solver_id=200  GROUP BY issue_id";
        $data=$this->pdo->query($sql);
        $data->execute();
        $result=$data->fetch(PDO::FETCH_ASSOC);
        return $result ;

    }

    public function summation(){
        $sql="select count(*) as total from issue_track";
        $data=$this->pdo->query($sql);

        $data->execute();
        $sum=$data->fetchAll(PDO::FETCH_ASSOC);

       return $sum[0];
    }


}