<?php
include_once('../session.php');
include_once('../header.php');
?>


    <div class="container">
        <div class="row main">


            <div class="main-form my-5" style="margin: 0px auto">


                <form class="form-horizontal" method="post" action="all_solving_record.php">


                    <div class="form-group">
                        <label for="from-date" class="cols-sm-2 control-label">From date</label>

                        <div class="input-group">
                            <input type="text" placeholder="yyy-mm-dd" id="fromDate" name="from_date"
                                   autocomplete="off">
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="to-date" class="cols-sm-2 control-label">To date</label>

                        <div class="input-group">

                            <input type="text" placeholder="yyy-mm-dd" id="toDate" name="to_date" autocomplete="off">
                        </div>

                    </div>


                    <div class="form-group ">
                        <button type="submit" id="solveRecord" name="submit" class="btn btn-primary ">Solve Record
                        </button>


                    </div>


                </form>
            </div>
        </div>
    </div>



<?php
include_once('../footer.php');
?>