<?php
require_once('Solver.php');
$obj = new Solver();

$id = $_GET['id'];
$resultArr = $obj->getIssueTrackInfoById($id);

$str = '<table class="table table-bordered">
        <thead>
        <tr>
            <th>Id</th>
            <th>issue_id</th>
            <th>progress</th>
            <th>remarks</th>
            <th>date</th>
        </tr>
        </thead>
        ';
foreach ($resultArr as $row) {
    $str .= '<tr> 
            <td>' . $row["id"] . '</td>
            <td>' . $row["issue_id"] . '</td>
            <td>' . $row["progress"] . '</td>
            <td>' . $row["remarks"] . '</td>
            <td>' . $row["date"] . '</td>
            </tr>';
}
$str .= '</table>';
echo $str;

?>


