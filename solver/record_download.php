<?php
require_once '../Classes/PHPExcel.php';
include_once('../session.php');
require_once('Solver.php');
$objPHPExcel = new PHPExcel();
$obj = new Solver();

if (isset($_POST) & count($_POST)) {
    $_SESSION['post'] = $_POST;
}

$dateArray = $obj->solveRecordByDate($_SESSION['post']);
//$allData=$obj->dateSearching($dateArray);
$solvingDate = $obj->solveCountQuery();

$solverDetails = $obj->getSolverWithCategory();

$subTotalArray = $obj->singleCount($solverDetails);

$summation = $obj->summation();

$countArray = $solverTotal = [];

$finalSum = 0;

foreach ($solvingDate as $item) {

    $countArray[$item['date']][$item['solver_id']] = $item['total'];
    $countArray[$item['date']]['net_total'] = isset($countArray[$item['date']]['net_total']) ? $countArray[$item['date']]['net_total'] : '0';
    $countArray[$item['date']]['net_total'] += $item['total'];

}


//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Solver_name!')
// ->setCellValue('B1', 'Category name');


$objExel = $objPHPExcel->setActiveSheetIndex(0);

$objExel->setCellValue('A1', 'solver name');
$objExel->setCellValue('B1', 'category name');


$c = 'C';
$str = 'C';
$a='A';

$total=0;
foreach ($dateArray as $date) {
    $objExel->setCellValue($c . '1', $date);
    $c++;
}

$subTotal=2;
$objExel->setCellValue($c . '1', 'sub total');
//$objExel->setCellValue($c . $subTotal++,$sum );


$i = 2;
$x = 2;
foreach ($solverDetails as $data) {

    $objExel->setCellValue('A' . $i, $data['solver_name']);
    $objExel->setCellValue('B' . $i, $data['category_name']);
    $i = $i + 1;
    $sum=0;
    foreach ($dateArray as $date) {

        if (isset($countArray[$date][$data['id']])) {
            $value = $countArray[$date][$data['id']];
            $objExel->setCellValue($str . $x, $value);
            $str++;
            $sum+=$value;

        } else {
            $objExel->setCellValue($str . $x, 0);
            $str++;
       }
    }
    $x++;
    $str = 'C';

    $objExel->setCellValue($c . $subTotal++,$sum );
    $total+=$sum;

}
$objExel->setCellValue($c . $subTotal++,$total );


$objExel->setCellValue($a.$i,'Total');

$endC='C';
foreach ($dateArray as $date){

    $finalResult= isset($countArray[$date]['net_total']) ?
        $countArray[$date]['net_total'] : '0';
        $objExel->setCellValue($endC.'5',$finalResult);
    $endC++;

}


//$objPHPExcel->getActiveSheet()->setTitle('Chesse1');


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="allRecord.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
