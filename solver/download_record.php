<?php
include_once('../session.php');
require_once('Solver.php');



$obj = new Solver();

if(isset($_POST) & count($_POST)) {
    $_SESSION['post'] = $_POST;
}
$dateArray = $obj->solveRecordByDate($_SESSION['post']);
//$allData=$obj->dateSearching($dateArray);
$solvingDate = $obj->solveCountQuery();

$solverDetails = $obj->getSolverWithCategory();

$subTotalArray = $obj->singleCount($solverDetails);

$summation = $obj->summation();

$countArray = $solverTotal = [];

$finalSum = 0;

foreach ($solvingDate as $item) {

    $countArray[$item['date']][$item['solver_id']] = $item['total'];
    $countArray[$item['date']]['net_total'] = isset($countArray[$item['date']]['net_total']) ? $countArray[$item['date']]['net_total'] : '0';
    $countArray[$item['date']]['net_total'] += $item['total'];


    //$solverTotal[$item['solver_id']] = isset($solverTotal[$item['solver_id']]) ? $solverTotal[$item['solver_id']] : '0';
    //$solverTotal[$item['solver_id']] += $item['total'];
    //echo '<pre>';
    //print_r($item);

}

?>

<div class="container">



    <table class="table table-striped" id="recordTable">
        <thead>
        <tr>
            <th>Solver Name</th>
            <th>Category Name</th>

            <?php foreach ($dateArray as $date) { ?>
                <th><?php echo $date; ?></th>

            <?php } ?>
            <th>Sub-Total</th>

        </tr>

        </thead>
        <tbody>

        <?php foreach ($solverDetails as $data) {

            ?>

            <tr>
                <td><?php echo $data['solver_name']; ?></td>
                <td><?php echo $data['category_name']; ?></td>

                <?php
                $sum = 0;
                foreach ($dateArray as $date) {
                    echo '<td>';
                    if (isset($countArray[$date][$data['id']])) {
                        echo '<p class="text-center">' .
                            $countArray[$date][$data['id']] .
                            '</p>';
                        $sum += $countArray[$date][$data['id']];
                    }
                    echo '</td>';
                }
                ?>

                <td><?php
                    //$subTotalArray['total'];
                    //$solverTotal[$item['id']];
                    echo $sum;
                    $finalSum += $sum;
                    ///$sum=0;


                    ?></td>
            </tr>

            <?php

        }
        ?>

        <tr>
            <td class="text-center bg-info" colspan="2">Total

            </td>

            <?php foreach ($dateArray as $date) { ?>
                <td class="text-center"><?php echo isset($countArray[$date]['net_total']) ?
                        $countArray[$date]['net_total'] : '0'; ?></td>
            <?php } ?>

            <td><?php echo $finalSum; ?></td>
        </tr>

        </tbody>
    </table>
</div>

<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=solvingRecord.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>