<?php
include_once('../session.php');
include_once('../header.php');
require_once('../issue/Issue.php');
$issue = new Issue();

if (!isset($_SESSION['userdata'])) {
    header('location:../signup/login.php');

}
if ($_SESSION['userdata']['group_id']==1){
    header('location:../signup/login.php');
}
if ($_SESSION['userdata']['group_id']==2){
    header('location:../signup/login.php');
}
//$find=$issue->index();
$data = $issue->joinTable();


//if ($_SESSION['userdata']['category_id']==$data['category_id']){
//    echo 'success';
//}
//else{
//    echo 'unsucess';
//}


?>
<body>

<div class="container">
    <h2>Issue Solve Management </h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Requester Name</th>
            <th>Category name</th>
            <th>Description</th>
            <th>Occured Date</th>
            <th>Status</th>
            <th>Manage</th>
            <th>Track</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $findAll) { ?>

            <tr>

                <td><?php echo $findAll['requester_name'] ?></td>
                <td><?php echo $findAll['category_name'] ?></td>
                <td><?php echo $findAll['description'] ?></td>
                <td><?php echo $findAll['date'] ?></td>
                <td><?php echo $findAll['situation'] ?></td>

                <td>
                    <a id="solveData-<?php echo $findAll['id']; ?>" class="solve-btn btn btn-success" href="#solution"
                       data-toggle="modal"> Solve</a>
                </td>

                <td>
                    <a id="issueTrack-<?php echo $findAll['id']; ?>" href="#solutionTrack"
                       class="track-btn btn btn-success"
                       data-toggle="modal">Issue Track</a>
                </td>

            </tr>

        <?php } ?>
        </tbody>
    </table>

</div>

<!!.. issue solve !!>
<div class="modal fade" id="solution" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Solution Progress</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post">
                    <input type="hidden" name="issue_id" id="issueId" value="">

                    <div class="form-group">
                        <label for="date" class="cols-sm-2 control-label">Date</label>

                        <div class="input-group">
                            <input type="text" id="solutionDate" name="date" autocomplete="off">
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="progress" class="cols-sm-2 control-label">Progress</label>
                        <input type="text" class="form-control" name="progress" id="progress"
                               placeholder=""/>

                    </div>

                    <div class="form-group">
                        <label for="remarks" class="cols-sm-2 control-label">Remarks</label>

                        <textarea class="form-control rounded-0" id="remarks" name="remarks" rows="3"></textarea>
                    </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="submit" class="btn btn-primary" data-dismiss="modal">Save</button>
            </div>
            </form>
        </div>

    </div>
</div>

<!! .. track modal !!>
<div class="modal fade" id="solutionTrack" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> your Solution Record</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container" id="allIssueRecord">

                </div>

            </div>

        </div>
    </div>
</div>

</body>
<?php
include_once('../footer.php');
?>
