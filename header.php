<?php
include_once('session.php');
include_once 'language.php';
//if (!isset($_SESSION['userdata'])) {
//    header('location:login.php');
//}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css" media="all">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <link rel="stylesheet" href="login_style.css" type="text/css">
    <link rel="stylesheet" href="../general_style.css" media="all">


</head>


<body>

<section class="header-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="logo-header">
                    <h2>START BOOTSTRAP</h2>
                </div>
            </div>
            <div class="col-md-9">
                <div class="menu">
                    <ul>


                        <li><a href="<?php echo BASE_PATH; ?>index.php">Home</a></li>

                        <?php /*if($_SESSION['userdata']['group_id'] == '1'){ */ ?>
                        <li><a href="<?php echo BASE_PATH; ?>signup/view.php">User</a></li>


                        <li><a href="<?php echo BASE_PATH; ?>category/category_view.php">Category</a></li>
                        <li><a href="<?php echo BASE_PATH;?>category/category_upload.php">Category upload</a></li>


                        <!-- --><?php /*} */ ?>
                        <?php /*if($_SESSION['userdata']['group_id'] == '2'){ */ ?>
                        <li><a href="<?php echo BASE_PATH; ?>issue/issue_view.php">Issue</a></li>
                        <!-- --><?php /*} */ ?>


                        <li><a href="<?php echo BASE_PATH; ?>solver/issue_solve.php">Solver</a></li>


                        <li><a href="<?php echo BASE_PATH; ?>signup/logout.php">Logout</a></li>

                    </ul>
                </div>
            </div>
        </div>

    </div>

</section>