<?php
include_once ('../session.php');
include_once ('../header.php');

require_once('Category.php');
$cat = new Category();
$data=$cat->view();


if(!isset($_SESSION['userdata'])){
    header('location:../signup/login.php');
}
if ($_SESSION['userdata']['group_id']==2){
    header('location:../signup/login.php');
}
if ($_SESSION['userdata']['group_id']==3){
    header('location:../signup/login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/bootstrap.min.css">


</head>
<body>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-6"><h2>Category Management</h2></div>
        <div class="col-md-6">
            <div class="category w-25" style="margin: 0px auto">
                <button class=" btn btn-primary">
                    <a href="category_create_form.php" class="text-white" style="text-decoration: none">ADD Category</a>
                </button>
            </div>

        </div>
    </div>


    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Id</th>
            <th>Category Name</th>
            <th>Category Description </th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $item){ ?>
        <tr>
            <td><?php echo $item['id']?></td>
            <td><?php echo $item['name']?></td>
            <td><?php echo $item['description']?></td>

            <td><a class="ml-2" href="category_details.php?cat_id=<?php echo $item['id']?>" style="text-decoration: none">
                    <i class="fas fa-info"></i>
                </a>

                <a class="ml-2" href="category_edit.php?cat_id=<?php echo $item['id']?>" style="text-decoration: none">
                    <i class="fas fa-edit"></i>
                </a>

                <a class="ml-2" href="category_delete.php?cat_id=<?php echo $item['id'] ?>" style="text-decoration: none">
                    <i class="fas fa-trash-alt"></i>

                </a>
            </td>

        </tr>
        <?php  }?>

        </tbody>
    </table>
</div>

</body>
</html>

<?php
include_once('../footer.php');

?>


