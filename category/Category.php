<?php

class Category{

    private $pdo=null;
    public $name="";
    public $description="";
    public $id="";



    public function __construct()
    {

        $this->pdo = new PDO('mysql:host=localhost;dbname=swapnoloke', 'root', '');

    }

    public function setData($data){

//            print_r($data);exit();

        if (!empty($data['name'])){
            $this->name=$data['name'];
        }
        if (!empty($data['description'])){
            $this->description=$data['description'];
        }
        if (!empty($data['id'])){
            $this->id=$data['id'];
        }




    }


    public function getData(){

        try{

            $query = "INSERT INTO category(name,description) VALUES
(:name,:description)";

            $ss = $this->pdo->prepare($query);


            $test = $ss->execute(array(
                ':name' => $this->name,
                ':description' => $this->description,
            ));
           if($test){
               header('location:category_view.php');
           }



        }catch (PDOException $e){
            echo 'error:0'.$e->getMessage();
        }


    }

    public function view(){
        try{

            $query="SELECT * FROM category";
            $data=$this->pdo->query($query);

            $data->execute();
            $test=$data->fetchAll(PDO::FETCH_ASSOC);
            return $test;



        }catch (PDOException $e){
            echo 'error:0'.$e->getMessage();
        }
    }

    public function singleView ($cat_id=''){
        try{

            $query="SELECT * FROM category where id=$cat_id";
            $data=$this->pdo->query($query);

            $test=$data->fetchAll(PDO::FETCH_ASSOC);



            return $test[0];



        }catch (PDOException $e){
            echo 'error:0'.$e->getMessage();
        }
    }

    public function update(){
        try{
            $query = "UPDATE category SET name=:name,description=:description where id=$this->id;";

//            echo $query;exit;

            $update = $this->pdo->prepare($query);



            $test=$update->execute(array(
                ':name' => $this->name,
                ':description' => $this->description,



            ));

            if ($test){
                header('location:category_view.php');
            }



        }catch (PDOException $e){
            echo 'error:0'.$e->getMessage();
        }
    }

        public function delete($delete=''){

        try{
            $query="DELETE FROM category where id=$delete";
            $test=$this->pdo->query($query);
            if ($test){
                header('location:category_view.php');
            }


        }catch (PDOException $e){
            echo 'error:0'.$e->getMessage();
        }

        }



}