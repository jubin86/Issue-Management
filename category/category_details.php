<?php
include_once ('../session.php');
include_once ('../header.php');
require_once ('Category.php');
$cat=new Category();
$category= $_GET['cat_id'];

$category_details=$cat->singleView($category);

if(!isset($_SESSION['userdata'])){
    header('location:../signup/login.php');
}
?>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-6">
            <div class="category w-25 mt-3">
                <button class=" btn btn-primary">
                    <a href="category_view.php" class="text-white">Back</a>
                </button>
            </div>

        </div>
    </div>
    <h2> Category of this issue</h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>iD</th>
            <th>Name</th>
            <th>Description</th>

        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php echo $category_details['id'];?></td>
            <td><?php echo $category_details['name'];?></td>
            <td><?php echo $category_details['description'];?></td>

</tr>


</tbody>
</table>
</div>

