<?php
include_once ('../session.php');
include_once ('../header.php');

require_once ('Category.php');
$cat=new Category();
$category= $_GET['cat_id'];

$edit=$cat->singleView($category);

if(!isset($_SESSION['userdata'])){
    header('location:../signup/login.php');
}

?>



    <body>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="category w-25 mt-3">
                    <button class=" btn btn-primary">
                        <a href="category_view.php" class="text-white">Back</a>
                    </button>
                </div>

            </div>
        </div>
        <div class="row main">


            <div class="main-form my-5" style="margin: 0px auto">


                <form class="form-horizontal" method="post" action="category_update.php">

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Category Name</label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>

                            <input type="text" class="form-control" name="name" id="name"
                               value="<?php echo $edit['name']?>"    placeholder="Category Name"/>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $edit['id']?>">

                    </div>

                    <div class="form-group">
                        <label for="email" class="cols-sm-2 control-label">Category description</label>


                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>


                        <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" name="description" rows="3"><?php echo $edit['description']?>
                        </textarea>


                    </div>





                    <div class="form-group ">
                        <button type="submit" name="submit" class="btn btn-primary ">Category Update</button>


                    </div>

                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    </body>
    </html>


<?php
include_once('../footer.php');

?>