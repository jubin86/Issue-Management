$(document).ready(function () {

    $(document).on('click', '.solve-btn', function () {
        var idArr = this.id.split('-');
        var id = idArr[1];
        $('#issueId').val(id);

    });

    $(document).on('click','.track-btn',function () {
       var idArr = this.id.split('-');

       var id= idArr[1];


        // var track_id= $('#issueTrack').val(id);
        $.ajax({
            url: "solver_index.php",
            data: {
                id: id,
            },
            type: 'GET',
            success: function(data){
                $("#allIssueRecord").html(data);
            },
            error: function (request, error) {
                console.log(request);
            },
        });


    });


    $(document).on('click', '#submit', function () {
        var issue_id = $("#issueId").val();
        var date = $('#solutionDate').val();
        var progress = $('#progress').val();

        var remarks = $('#remarks').val();

        if (date == '') {
            alert('Please, insert date');
            return false;
        }
        if (progress == '') {
            alert('Please, insert progress');
            return false;
        }


        $.ajax({
            url: "solver_progress.php",
            data: {
                issue_id: issue_id,
                date: date,
                progress: progress,
                remarks: remarks,
            },
            type: 'POST',
        });
    });

    $('#solutionDate').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });


    $(document).on('change', '#groupId', function () {

        var groupId= $('#groupId').val();
        if (groupId==3){
            $("#showCategory").show("slow");
        }else {
            $("#showCategory").hide("slow");
        }


    });

    /*$("#solverId").click(function(){
        $("#categoryId").hide();
    });
    $("#solverId").click(function(){
        $("#categoryId").show();
    });*/

    $(document).on('click','.issue-status',function () {

            var idArray= this.id .split('-');
            var id=idArray[1];

            $.ajax({
                url:'issue_solution.php',
                data:{
                    id:id,
                },
                type: 'GET',
                success:function (data) {
                    $("#solutionView").html(data);
                },

            });

    });


    /*$(document).on('click','#solveRecord',function () {

        var fromDate=$("#fromDate").val();
        var toDate=$('#toDate').val();



        if (fromDate == ''){
            alert('please give your start date');
            return false;
        }
        if (toDate == ''){
            alert('Please give your end date');
            return false;
        }

        $.ajax({
            url:'all_solving_record.php',
            data:{
                fromDate:fromDate,
                toDate:toDate,
            },
            type:'POST',
            // success:function(){
            //     window.location.href = 'all_solving_record.php';
            // }

        });


    });
*/

    $('#fromDate,#toDate').datepicker({
        dateFormat:'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });





});
