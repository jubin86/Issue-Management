<?php
include_once('../session.php');
include_once('../header.php');
require_once('Issue.php');
$issue = new Issue();
$issue_id = $_GET['id'];

/*$find=$issue->index();*/


$data = $issue->details($issue_id);




if (!isset($_SESSION['userdata'])) {
    header('location:login.php');
}

?>


<div class="container mt-5">
    <div class="row">
        <div class="col-md-6">
            <div class="category w-25 mt-3">
                <button class=" btn btn-primary">
                    <a href="issue_view.php" class="text-white">Back</a>
                </button>
            </div>

        </div>
    </div>
    <h2> Information of this issue</h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Categoryname</th>
            <th>Description</th>
            <th>Occured Day</th>
            <th>Situation Level</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php echo $data['category_name'];?></td>
            <td><?php echo $data['description'];?></td>
            <td><?php echo $data['date'];?></td>
            <td><?php echo $data['situation'];?></td>
        </tr>


        </tbody>
    </table>
</div>



