
<?php
include_once('../session.php');

class Issue
{

    private $pdo = null;
    public $category_id = "";
    public $description = "";
    public $date = "";
    public $situation = "";
    public $id = "";


    public function __construct()
    {

        $this->pdo = new PDO('mysql:host=localhost;dbname=swapnoloke', 'root', '');

    }

    public function setData($data)
    {
//        $rawdate = htmlentities($_POST['date']);
//            print_r($data);exit();

        if (!empty($data['category_id'])) {
            $this->category_id = $data['category_id'];
        }
        if (!empty($data['description'])) {
            $this->description = $data['description'];
        }
        if (!empty($data['date'])) {
            $this->date = htmlentities($data['date']);
        }
        if (!empty($data['situation'])) {
            $this->situation = $data['situation'];
        }

        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }


    }


    public function insertData()
    {
//        echo '<pre>';
//        print_r($_SESSION);exit;
        try {


            $query = "INSERT INTO issue(category_id,description,date,situation,created_by) VALUES
                    (:category_id,:description,:date,:situation,:creatd_by)";

            $ss = $this->pdo->prepare($query);


            $test = $ss->execute(array(
                ':category_id' => $this->category_id,
                ':description' => $this->description,
                ':date' => $this->date,
                ':situation' => $this->situation,
                ':creatd_by' => $_SESSION['userdata']['id'],

            ));
            if ($test) {
                header('location:issue_view.php');
            }


        } catch (PDOException $e) {
            echo 'error:0' . $e->getMessage();
        }


    }

    public function singleView($id=''){
        try{

            $query="SELECT * FROM issue where id=$id";
            $data=$this->pdo->query($query);

            $data->execute();
            $test=$data->fetchAll(PDO::FETCH_ASSOC);

            return $test[0];



        }catch (PDOException $e){
            echo 'error:0'.$e->getMessage();
        }
    }
    public function index()
    {

        $query = "SELECT issue.*, category.name as category_name FROM issue inner join category on 
issue.category_id = category.id";

        $stmt = $this->pdo->prepare($query);

        $stmt->execute();
        $findall = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $findall;


    }

    public function details($id = '')
    {
        try {
            $query="SELECT issue . *,category.name as category_name FROM issue inner join 
category on issue.category_id=category.id where issue.id=$id limit 1";


            //echo $query;exit;
            $data = $this->pdo->query($query);

            $test = $data->fetchAll(PDO::FETCH_ASSOC);

            return $test[0];


        } catch (PDOException $e) {
            echo 'error:0' . $e->getMessage();
        }
    }

    public function update()
    {
        try {
            $query = "UPDATE issue SET category_id=:category_id,date=:date,situation=:situation
              where id=$this->id;";

//            echo $query;exit;

            $update = $this->pdo->prepare($query);


            $test = $update->execute(array(
                ':category_id' => $this->category_id,
                ':date' => $this->date,
                ':situation' => $this->situation,


            ));

            if ($test) {
                header('location:issue_view.php');
            }


        } catch (PDOException $e) {
            echo 'error:0' . $e->getMessage();
        }
    }

    public function delete($delete = '')
    {

        try {
            $query = "DELETE FROM issue where id=$delete LIMIT 1";
            $test = $this->pdo->query($query);
            if ($test) {
                header('location:issue_view.php');
            }


        } catch (PDOException $e) {
            echo 'error:0' . $e->getMessage();
        }

    }

    /*public function checkIssueAuthorized()
    {

        $user = new User();
        $test = $user->pathLoginUser($value = '');
        echo '<pre>';
        print_r($test);

    }*/

    public  function joinTable(){
        $query="SELECT issue.*,registration.name as requester_name,category.name as category_name FROM issue  inner join 
registration on issue.created_by=registration.id 

inner join category on issue.category_id=category.id where category.id = ".$_SESSION['userdata']['category_id'];


        $stmt = $this->pdo->prepare($query);

        $stmt->execute();
        $findall = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $findall;
    }





}