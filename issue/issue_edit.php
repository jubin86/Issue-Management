<?php
include_once ('../session.php');
include_once('../header.php');
require_once('Issue.php');
include_once ('../category/Category.php');

$cat=new Category();
$issue = new Issue();

$edit_id = $_GET['id'];

$specific_issue = $issue->singleView($edit_id);


$categoryArr = $cat->view();


if(!isset($_SESSION['userdata'])){
    header('location:../signup/login.php');
}

?>


    <body>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="category w-25 mt-3">
                    <button class=" btn btn-primary">
                        <a href="issue_view.php" class="text-white">Back</a>
                    </button>
                </div>

            </div>
        </div>
        <div class="row main">


            <div class="main-form my-5" style="margin: 0px auto">
                <form class="form-horizontal" method="post" action="issue_update.php">

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Category Select</label>

                        <div class="input-group">

                            <select name="category_id" id="name" class="form-control">
                                <option value="0">select</option>
                                <?php
                                foreach ($categoryArr as $category) {
                                    $selected = ($category['id'] == $specific_issue['category_id']) ? 'selected="selected"' : '';
//                                    if($category['name'] == $specific_issue['category_name']){
//                                        $selected = 'selected="selected"';
//                                    }else{
//                                        $selected = '';
//                                    }
                                    echo '<option value="' . $category['id'] . '" '.$selected.'>' . $category['name'] . '</option>';
                                }

                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Occured day</label>

                        <div class="input-group">

                            <input type="date" class="form-control" name="date" value="<?php echo $specific_issue['date']?>" id="name">
                        </div>

                    </div>


                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Emmergency level</label>

                        <div class="input-group">
                            <select name="situation" id="" class="form-control">
                                <option value="0">select</option>
                                <option value="critical"<?php echo  ($specific_issue['situation']=='critical'? 'selected="selected"': '')?>>critical</option>
                                <option value="high" <?php echo ($specific_issue['situation']=='high'? 'selected="selected"': '')?>>high</option>
                                <option value="low" <?php echo ($specific_issue['situation']=='low'? 'selected="selected"': '')?>>low</option>
                            </select>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $specific_issue['id']?>">

                    </div>


                    <div class="form-group ">
                        <button type="submit" name="submit" class="btn btn-primary ">Category Update</button>


                    </div>

                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    </body>
    </html>


<?php


include_once('../footer.php');

?>