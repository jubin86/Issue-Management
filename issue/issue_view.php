<?php
include_once('../session.php');
include_once('../header.php');
include_once('../signup/user.php');
require_once('Issue.php');

$issue = new Issue();
$user = new User();


$data = $issue->index();


//$data=$issue->view();
//echo '<pre>';
//print_r($data);

if (!isset($_SESSION['userdata'])) {
    header('location:../signup/login.php');


}
if ($_SESSION['userdata']['group_id'] == 1) {
    header('location:../signup/login.php');
}
if ($_SESSION['userdata']['group_id'] == 3) {
    header('location:../signup/login.php');
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/bootstrap.min.css">


</head>
<body>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-6"><h2>Issue Management</h2></div>
        <div class="col-md-6">
            <div class="category w-25" style="margin: 0px auto">
                <button class=" btn btn-primary">
                    <a href="issue_create_form.php" class="text-white" style="text-decoration: none">ADD Issue</a>
                </button>
            </div>

        </div>
    </div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Id</th>
            <th>Category Name</th>
            <th>description</th>
            <th>Occured date</th>
            <th> situation</th>
            <th>status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $alldata) { ?>
            <tr>
                <td><?php echo $alldata['id'] ?></td>
                <td><?php echo $alldata['category_name'] ?></td>
                <td><?php echo $alldata['description'] ?></td>
                <td><?php echo $alldata['date'] ?></td>
                <td><?php echo $alldata['situation'] ?></td>


                <td><a class="ml-2" href="issue_details.php?id=<?php echo $alldata['id'] ?>" style="text-decoration: none">
                        <i class="fas fa-info"></i>
                    </a>

                    <a class="ml-2" href="issue_edit.php?id=<?php echo $alldata['id'] ?>" style="text-decoration: none">
                        <i class="fas fa-edit"></i>
                    </a>

                    <a class="ml-2" href="issue_delete.php?id=<?php echo $alldata['id'] ?>" style="text-decoration: none">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                </td>
                <td>
                    <a id="issueStatus-<?php echo $alldata['id']; ?>" class="issue-status btn btn-success"
                       href="#issuesolution" data-toggle="modal">
                        pending
                    </a>
                </td>

            </tr>
        <?php } ?>

        </tbody>
    </table>
</div>

<div class="modal fade" id="issuesolution" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> Your Solution Record</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container" id="solutionView">

                </div>

            </div>

        </div>
    </div>
</div>

</body>
</html>

<?php
include_once('../footer.php');

?>
